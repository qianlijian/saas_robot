$(document).ready(function () {

    'use strict';

    /***** USING OTHER SYMBOLS *****/

    var firefox = [
        [0, 60],
        [1, 80],
        [2, 60],
        [3, 110],
        [4, 70],
        [5, 130],
        [6, 90],
        [7, 80],
        [8, 100],
        [9, 890],
        [10, 130],
        [11, 103],
        [12, 2130],
        [13, 103],
        [14, 130],
        [15, 103],
        [16, 1130],
        [17, 103],
        [18, 130],
        [19, 103],
        [20, 130],
        [21, 103],
        [22, 130],
        [23, 103]
    ];
    var chrome = [
        [0, 600],
        [1, 800],
        [2, 600],
        [3, 1100],
        [4, 700],
        [5, 1300],
        [6, 900],
        [7, 800],
        [8, 1000],
        [9, 900],
        [10, 1230],
        [11, 1300],
        [12, 1130],
        [13, 1300],
        [14, 1300],
        [15, 1300],
        [16, 1130],
        [17, 1300],
        [18, 1230],
        [19, 1300],
        [20, 1130],
        [21, 1300],
        [22, 1230],
        [23, 1130]
    ];

    var plot2 = $.plot($('#basicflot2'),
        [{
            data: firefox,
            label: '运行次数(单位-次)',
            color: '#D9534F',
            points: {
                symbol: 'square'
            }
        },
        {
            data: chrome,
            label: '交易总量(单位-千)',
            color: '#428BCA',
            lines: {
                fill: true
            },
            points: {
                symbol: 'diamond',
                lineWidth: 2
            }
        }
        ],
        {
            series: {
                lines: {
                    show: true,
                    lineWidth: 2
                },
                points: {
                    show: true
                },
                shadowSize: 0
            },
            legend: {
                position: 'nw'
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderColor: '#ddd',
                borderWidth: 1,
                labelMargin: 10,
                backgroundColor: '#fff'
            },
            yaxis: {
                min: 0,
                max: 5000,
                color: '#eee'
            },
            xaxis: {
                color: '#eee',
                min: 0,
                max: 23
            }
        });

    var previousPoint2 = null;
    $('#basicflot2').bind('plothover', function (event, pos, item) {
        $('#x').text(pos.x.toFixed(2));
        $('#y').text(pos.y.toFixed(2));

        if (item) {
            if (previousPoint2 != item.dataIndex) {
                previousPoint2 = item.dataIndex;

                $('#tooltip').remove();
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

                // showTooltip(item.pageX, item.pageY,
                // item.series.label + ' of ' + x + ' = ' + y);
            }

        } else {
            $('#tooltip').remove();
            previousPoint2 = null;
        }

    });

    $('#basicflot2').bind('plotclick', function (event, pos, item) {
        if (item) {
            plot2.highlight(item.series, item.datapoint);
        }
    });




});
