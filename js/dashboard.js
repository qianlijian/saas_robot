$(document).ready(function () {

	'use strict';

	var m1 = new Morris.Line({
		// ID of the element in which to draw the chart.
		element: 'line-chart',
		// Chart data records -- each entry in this array corresponds to a point on
		// the chart.
		data: [
			{ y: '2019-10-01', a: 100, b: 10 },
			{ y: '2019-10-02', a: 230, b: 23 },
			{ y: '2019-10-03', a: 290, b: 29 },
			{ y: '2019-10-04', a: 390, b: 99 },
			{ y: '2019-10-05', a: 410, b: 41 },
			{ y: '2019-10-06', a: 110, b: 10 },
			{ y: '2019-10-07', a: 134, b: 13 },
		],
		xkey: 'y',
		ykeys: ['a', 'b'],
		labels: ['本日消耗', '机器人运行时间'],
		lineColors: ['#1CAF9A', '#ff564e'],
		lineWidth: '1px',
		fillOpacity: 0.2,
		smooth: false,
		hideHover: true
	});


	// Tooltip for flot chart
	function showTooltip(x, y, contents) {
		$('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css({
			position: 'absolute',
			display: 'none',
			top: y + 5,
			left: x + 5
		}).appendTo('body').fadeIn(200);
	}
	//报错
	var lognum = [
		[0, 0],
		[1, 1],
		[2, 2],
		[3, 0],
		[4, 1],
		[5, 5],
		[6, 12],
		[7, 5],
		[8, 2],
		[9, 0],
		[10, 0],
		[11, 0],
		[12, 0],
		[13, 0],
		[14, 0],
		[15, 0],
		[16, 0],
		[17, 0],
		[18, 0],
		[19, 0],
		[20, 0],
		[21, 0],
		[22, 0],
		[23, 0]
	];
	//进程
	var jinchen = [
		[0, 10],
		[1, 12],
		[2, 12],
		[3, 12],
		[4, 12],
		[5, 15],
		[6, 12],
		[7, 15],
		[8, 12],
		[9, 10],
		[10, 10],
		[11, 10],
		[12, 10],
		[13, 10],
		[14, 10],
		[15, 20],
		[16, 30],
		[17, 10],
		[18, 10],
		[19, 10],
		[20, 10],
		[21, 10],
		[22, 10],
		[23, 10]
	];

	var plot = $.plot($('#basicflot'), [{
		data: lognum,
		label: '报错',
		color: '#cfd3da'
	},
	{
		data: jinchen,
		label: '进程',
		color: '#06b5cf',
	}],
		{
			series: {
				lines: {
					show: false,
				},

				splines: {
					show: true,
					tension: 0.3,
					lineWidth: 2,
					fill: .50
				},

				shadowSize: 0
			},

			points: { show: true },

			legend: {
				container: '#basicFlotLegend',
				noColumns: 0
			},

			grid: {
				hoverable: true,
				clickable: true,
				borderColor: '#f3f5f7',
				borderWidth: 0,
				labelMargin: 5
			},

			yaxis: {
				min: 0,
				max: 100,
				color: '#f3f5f7'
			},

			xaxis: { color: '#f3f5f7' }

		});

	var previousPoint = null;

	$('#basicflot').bind('plothover', function (event, pos, item) {
		$('#x').text(pos.x.toFixed(2));
		$('#y').text(pos.y.toFixed(2));

		if (item) {
			if (previousPoint != item.dataIndex) {
				previousPoint = item.dataIndex;

				$('#tooltip').remove();
				var x = item.datapoint[0].toFixed(2),
					y = item.datapoint[1].toFixed(2);

				showTooltip(item.pageX, item.pageY, item.series.label + ' of ' + x + ' = ' + y);
			}

		} else {
			$('#tooltip').remove();
			previousPoint = null;
		}
	});

	$('#basicflot').bind('plotclick', function (event, pos, item) {
		if (item) {
			plot.highlight(item.series, item.datapoint);
		}
	});

	// Knob
	$('.dial-success').knob({
		readOnly: true,
		width: '70px',
		bgColor: '#E7E9EE',
		fgColor: '#259CAB',
		inputColor: '#262B36'
	});

	$('.dial-danger').knob({
		readOnly: true,
		width: '70px',
		bgColor: '#E7E9EE',
		fgColor: '#D9534F',
		inputColor: '#262B36'
	});

	$('.dial-info').knob({
		readOnly: true,
		width: '70px',
		bgColor: '#66BAC4',
		fgColor: '#fff',
		inputColor: '#fff'
	});

	$('.dial-warning').knob({
		readOnly: true,
		width: '70px',
		bgColor: '#E48684',
		fgColor: '#fff',
		inputColor: '#fff'
	});


});
